525 Assignment1

Team: Yanjia Xu, Luwei Zhang, Shuting Yin

GOAL: implement a simple storage manager - a module that is capable of reading blocks from a file on disk into memory and writing blocks from memory to a file on disk.

1.storage_mgr.h: the provided header file 
2.storage_mgr.c: the file implement methods.

  1)First, we should do some basic operation about file, creating a file, opening the       existing file, closing this file and destroying this file.

  *createPageFile(): open for writing a binary file, this method initialized a single       page with zero bytes.
  *openPageFile(): open for reading and writing a binary file. first we should check if    this file can open or not. if it can open successfully, then the file handle should    be initialized with the information about the opened file.
  *closePageFile(): close this page file
  *destroyPageFile(): delete this opened page file.

  2)Second, we should read blocks from a file on disk into memory
  *readBlock(): this method reads the absolute block from a file and stores its content    in the memory pointed to by the memPage page handle. 
  *getBlockPos(): get the current page position of the file
  *readFirstBlock(),readLastBlock():read the first and last page of the file
  *readPreviousBlock(),readCurrentBlock(),readNextBlock(): use curPagePos to read the       current, previous, and next page of the file

  3)Third, we should write blocks from memory to a file on disk
  *writeBlock(),writeCurrentBlock(): write page to disk
  *appendEmptyBlock(): increase the number of pages in the file by one
  *ensureCapacity(): increase the size to numberOfPages if the fils has more than       numberOfPages pages.

  